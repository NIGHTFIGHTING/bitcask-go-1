package data

import (
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func TestOpenDataFile(t *testing.T) {
	file, err := OpenDataFile(os.TempDir(), 1)
	assert.Nil(t, err)
	assert.NotNil(t, file)
}

func TestDataFile_Write(t *testing.T) {
	file, err := OpenDataFile(os.TempDir(), 1)
	assert.Nil(t, err)
	assert.NotNil(t, file)

	err = file.Write([]byte("leisurexi"))
	assert.Nil(t, err)
}

func TestDataFile_Sync(t *testing.T) {
	file, err := OpenDataFile(os.TempDir(), 1)
	assert.Nil(t, err)
	assert.NotNil(t, file)

	err = file.Write([]byte("leisurexi"))
	assert.Nil(t, err)

	err = file.Sync()
	assert.Nil(t, err)
}

func TestDataFile_Close(t *testing.T) {
	file, err := OpenDataFile(os.TempDir(), 1)
	assert.Nil(t, err)
	assert.NotNil(t, file)

	err = file.Write([]byte("leisurexi"))
	assert.Nil(t, err)

	err = file.Close()
	assert.Nil(t, err)
}

func TestDataFile_ReadLogRecord(t *testing.T) {
	dataFile, err := OpenDataFile(os.TempDir(), 1)
	assert.Nil(t, err)
	assert.NotNil(t, dataFile)

	record := &LogRecord{
		Key:   []byte("name"),
		Value: []byte("leisurexi"),
		Type:  LogRecordNormal,
	}
	bytes, size := EncodeLogRecord(record)

	err = dataFile.Write(bytes)
	assert.Nil(t, err)

	logRecord, readSize, err := dataFile.ReadLogRecord(0)
	assert.Nil(t, err)
	assert.Equal(t, size, readSize)
	t.Log(logRecord)
	t.Log(readSize)

	logRecord2, readSize2, err := dataFile.ReadLogRecord(readSize)
	assert.Nil(t, err)
	assert.Equal(t, size, readSize)
	t.Log(logRecord2)
	t.Log(readSize2)
}
