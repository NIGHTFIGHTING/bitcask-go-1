package index

import (
	"bitcask-go/data"
	"github.com/stretchr/testify/assert"
	"testing"
)

// 测试库: https://github.com/stretchr/testify

func TestBTree_Put(t *testing.T) {
	bTree := NewBTree()
	res1 := bTree.Put(nil, &data.LogRecordPos{Fid: 1, Offset: 100})
	assert.True(t, res1)

	res2 := bTree.Put([]byte("leisurexi"), &data.LogRecordPos{Fid: 1, Offset: 100})
	assert.True(t, res2)
}

func TestBTree_Get(t *testing.T) {
	bTree := NewBTree()
	res1 := bTree.Put(nil, &data.LogRecordPos{Fid: 1, Offset: 100})
	assert.True(t, res1)

	pos1 := bTree.Get(nil)
	assert.Equal(t, uint32(1), pos1.Fid)
	assert.Equal(t, int64(100), pos1.Offset)

	res2 := bTree.Put([]byte("leisurexi"), &data.LogRecordPos{Fid: 1, Offset: 2})
	assert.True(t, res2)
	res3 := bTree.Put([]byte("leisurexi"), &data.LogRecordPos{Fid: 1, Offset: 3})
	assert.True(t, res3)

	pos2 := bTree.Get([]byte("leisurexi"))
	assert.Equal(t, uint32(1), pos2.Fid)
	assert.Equal(t, int64(3), pos2.Offset)
}

func TestBTree_Delete(t *testing.T) {
	bTree := NewBTree()
	res1 := bTree.Put(nil, &data.LogRecordPos{Fid: 1, Offset: 100})
	assert.True(t, res1)
	res2 := bTree.Delete(nil)
	assert.True(t, res2)

	res3 := bTree.Put([]byte("leisurexi"), &data.LogRecordPos{Fid: 1, Offset: 100})
	assert.True(t, res3)
	res4 := bTree.Delete([]byte("leisurexi"))
	assert.True(t, res4)
}
